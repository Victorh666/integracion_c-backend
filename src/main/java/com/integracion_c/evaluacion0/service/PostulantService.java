package com.integracion_c.evaluacion0.service;

import com.integracion_c.evaluacion0.interfase.IPostulant;
import com.integracion_c.evaluacion0.model.Postulant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PostulantService {
    @Autowired
    private IPostulant iPostulant;

    public Iterable<Postulant> list(){
        return iPostulant.findAll();
    }

    public Optional<Postulant> listById(int id) {
        return iPostulant.findById(id);
    }

    public Postulant savePostulant(Postulant postulant){
        return iPostulant.save(postulant);
    }

}
