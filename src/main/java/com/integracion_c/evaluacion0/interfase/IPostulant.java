package com.integracion_c.evaluacion0.interfase;

import com.integracion_c.evaluacion0.model.Postulant;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IPostulant extends CrudRepository<Postulant, Integer> {

}
