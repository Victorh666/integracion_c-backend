package com.integracion_c.evaluacion0.controller;

import com.integracion_c.evaluacion0.model.Postulant;
import com.integracion_c.evaluacion0.service.PostulantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@CrossOrigin("*")
@RestController
public class PostulantController {
    @Autowired
    PostulantService postulantService;

    @GetMapping("/postulant/list")
    public Iterable<Postulant> listAllPostulant(){
        return postulantService.list();
    }

    @GetMapping("/postulant/{id}")
    public Postulant postulantById(@PathVariable("id") int id){
        Optional<Postulant> postulant = postulantService.listById(id);
        if (postulant.isPresent()){
            return postulant.get();
        }
        return null;
    }

    /*
    @GetMapping("/postulant/careers/{id}")
    public List<String> postulantCareers(@PathVariable("id") int id){
        Optional<Postulant> postulant = postulantService.listById(id);
        if (postulant.isPresent()){
            return postulant.get().getCareersSelectedList();
        }
        return null;
    }
     */
    @PostMapping("/postulant/create")
    public int save(@RequestBody Postulant postulant){
        postulantService.savePostulant(postulant);
        return postulant.getId();
    }

    @GetMapping("/hellow")
    public String hellow() {
        return "Hola mundo!";
    }
}
