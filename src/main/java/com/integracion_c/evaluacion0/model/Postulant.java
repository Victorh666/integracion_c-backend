package com.integracion_c.evaluacion0.model;
import javax.persistence.*;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Entity
public class Postulant {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="id")
    private int id;

    @Column(name="name")
    private String name;

    @Column(name="rut")
    private String rut;

    @Column(name="birthDate")
    private Date birthDate;

    @Column(name="careersSelected")
    private String careersSelected;

    public Postulant() {
    }

    public Postulant(int id, String name, String rut, Date birthDate, String careersSelected) {
        this.id = id;
        this.name = name;
        this.rut = rut;
        this.birthDate = birthDate;
        this.careersSelected = careersSelected;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getCareersSelected() {
        return careersSelected;
    }

    public void setCareersSelected(String careersSelected) {
        this.careersSelected = careersSelected;
    }

    /*public List<String> getCareersSelectedList() {
        String careers[] = this.careersSelected.split(",");
        List<String> careersList = new ArrayList<String>();
        careersList = Arrays.asList(careers);
        return careersList;
    }
     */
}

