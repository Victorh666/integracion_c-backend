package com.integracion_c.evaluacion0;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Evaluacion0Application {

    public static void main(String[] args) {
        SpringApplication.run(Evaluacion0Application.class, args);
    }

}
